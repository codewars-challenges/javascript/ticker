const ticker = (text, width, tick) => {
    let e = tick % (text.length + width);
    let s = Math.max(e - width, 0);
    let t = (s === 0) ? text.substring(s, e).padStart(Math.min(width, width + text.length - tick % (width + text.length))) : text.substring(s, e);
    return (t.length < width) ? t.padEnd(width) : t;
};


for (let i = 0; i < 34; i++) {
    console.log(ticker('Smaller', 10, i));
}

for (let i = 0; i < 34; i++) {
    console.log(ticker('LongerMessage', 10, i));
}

for (let i = 0; i < 34; i++) {
    console.log(ticker('SameLength', 10, i));
}

